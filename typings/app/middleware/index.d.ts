// This file is created by egg-ts-helper@1.25.6
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportReqLog = require('../../../app/middleware/reqLog');

declare module 'egg' {
  interface IMiddleware {
    reqLog: typeof ExportReqLog;
  }
}
