// This file is created by egg-ts-helper@1.25.6
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportBase = require('../../../app/controller/base');
import ExportFySplitAccount = require('../../../app/controller/fySplitAccount');
import ExportHome = require('../../../app/controller/home');
import ExportIndex = require('../../../app/controller/index');

declare module 'egg' {
  interface IController {
    base: ExportBase;
    fySplitAccount: ExportFySplitAccount;
    home: ExportHome;
    index: ExportIndex;
  }
}
