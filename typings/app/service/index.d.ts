// This file is created by egg-ts-helper@1.25.6
// Do not modify this file!!!!!!!!!

import 'egg';
import ExportCommon = require('../../../app/service/common');
import ExportCustomer = require('../../../app/service/customer');
import ExportFuyou = require('../../../app/service/fuyou');
import ExportFuyouSplitAccount = require('../../../app/service/fuyouSplitAccount');
import ExportOrder = require('../../../app/service/order');
import ExportSplitLog = require('../../../app/service/splitLog');

declare module 'egg' {
  interface IService {
    common: ExportCommon;
    customer: ExportCustomer;
    fuyou: ExportFuyou;
    fuyouSplitAccount: ExportFuyouSplitAccount;
    order: ExportOrder;
    splitLog: ExportSplitLog;
  }
}
