'use strict';
module.exports = app => {
  const exports = {};
  exports.cluster = {
    listen: {
      port: 7005,
      hostname: '127.0.0.1',
    },
  };
  return exports;
};