/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
	/**
	 * built-in config
	 * @type {Egg.EggAppConfig}
	 **/
	const config = exports = {};

	// use for cookie sign key, should change to your own and keep security
	config.keys = appInfo.name + '_1570435966820_422';

	// add your middleware config here
	config.middleware = [];

	// add your user config here
	const userConfig = {
		// myAppName: 'egg',
	};

	config.mysql = {
		// 单数据库信息配置
		client: {
			// host
			host: 'localhost',
			// 端口号
			port: '3306',
			// 用户名
			user: 'root',
			// 密码
			password: '666666',
			// 数据库名
			database: 'thinkpay',
			//host: 'rm-wz979m01u5k505j6m4o.mysql.rds.aliyuncs.com',
			//password: 'ZHAOpeng1314520',
			//database: 'my',
			timezone: '08:00',
			dateStrings: true, // 要配置此项，否则就是UTC时间
		},
		// 是否加载到 app 上，默认开启
		app: true,
		// 是否加载到 agent 上，默认关闭
		agent: false,
	};

	// 关闭csrf
	config.security = {
		csrf: {
			enable: false,
		},
	};
	
	config.fuyou = {
		rsaKey : {
			privateKey: 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIuf1WM1OyUfUm79YoyA6dk9gY84iwu1kHHmbQ0/zUqaVjYZpQ7Dfs3MazZk/ESwQTClxOfrO7uQTohpxihHurtd4hEWRTb8RvTRHoezLeyTe46QiV/Icmlh9Yq2XZnRXv4LW7+MuM5e25jmJLYhtyvbyKsYJ7wO3mvD1Rud6PQXAgMBAAECgYBhLDULnaFzHZB2dFyg4ptduOOxdK240X0c+DyPSwcqU62Ka2CnnMFko08+hRvZBj3oiIY4zks9J5qwb8UchYdcFWGzA26J6edY5G5cU/FVBcFy/TZ6gFTSFwqTJWEHNkpQPJwjLkdjTJ9LHPywJ803Rtg4ckd/l4U0stAiwtqOYQJBAMzct5e1pv7yPLvF/2wckbFiOBtEJXxrjb7Z6vUPgqT7yak0YIyaaA2PitZokizCTdesXKBwq7Qj9LECthf4Z8cCQQCuejtApOqxkcqzrwq8sGVpfyRYJ6I3grjLXSr6WIKMPOl1zTuMZh7yWBK6ycGKlqhv41pejFL0Ug6K98SogDExAkEAuvaYq2FCRhyHZwtdf6pXIN81u1SnLXTvCQXSk9KS9cVBFyZREyD745xkJieLeEPgTPvmERqi6aWC9GU4EIpJEwJAFzWvWCQYDFMGkeyBEySr+OKu5QMwsw2yo2rbYr4Pb7RSohcRCtYy264Xn71i/5qXAXtO9DDagCHCSX44LOoQoQJAJ7me6O8qRr7LxZwCjVeTgGhvF6MvShZWgY242fV29FGlkdPZZTEqlKoM/qxZmMF+bvOhmj8+qaADITGu5X7GSA==',
			publicKey: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCCqKGAM+R56ykxkhlC3WiM4EyeRhQx8ItHfHs8lMk2p7p9v+PXtEI/VR92ECP2eB71rdjtjaHOWZm0bK8ILRh1IAJymfxE4xHx4S01uBDNMvxoulk2uLMRP1VVOPHCxh6hnCNTZnhLHiZmAZjSkERVHM9vnk3nzz/4g37t/bszwwIDAQAB'
		},
		reqHost: "https://spay-xs.fuioupay.com",       // 正式
		apiOrderCreateUrl: '/wxPreCreate',
		apiOrderRefundUrl: '/commonRefund',
		parameters: {
			version: '1',
			ins_cd: '08M0026945',            // 机构号,接入机构在富友的唯一代码
//			mchnt_cd: '0006510F2756892',     // 商户号, 富友分配给二级商户的商户号
			term_id: '88888888',             // 终端号(没有真实终端号统一填88888888)
//			trade_type: 'JSAPI',             //订单类型:JSAPI--公众号支付、FWC--支付宝服务窗、LETPAY-小程序、BESTPAY--翼支付js
		},
		splitAccount: {
			ver: '0.4',
//			mchntCd: '0002900F8005508', //测试商户号
//			reqHost: 'http://180.168.100.155:8069', //测试域名
//			createCompanyUrl: '/sub-account/splitUser/addUser.html', // 测试地址
//			splitRuleCreate: '/sub-account/splitRule/addRule.html', // 测试地址
//			orderSplit: '/sub-account/balance/subaccount.html', // 测试地址
//			orderSplitCancel: '/sub-account/balance/cancelSub.html', // 测试地址
			mchntCd: '0006510F2842089', //润雄商户号
			reqHost: 'https://suba.fuioupay.com', // 正式环境1
//			reqHost: 'https://suba-xs.fuioupay.com', // 正式环境2
			createCompanyUrl: '/splitUser/addUser.html', // 正式地址
			splitRuleCreate: '/splitRule/addRule.html', // 正式地址
			orderSplit: '/balance/subaccount.html', // 正式地址
			orderSplitCancelL: '/balance/cancelSub.html', // 正式地址
			setWxConfig : '/wxConfig/setWxConfig.html', 
			wxPay: '/pay/wxPay.html',
			rsaKey: {
//				// 测试
//				privateKey: 'MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBANNT0cbtsIpyzla/ujfER3Lb3EhDVSFe1L/dLtWqUoyoDdhIi+fP6X4OdrAt9Tg8vrLqOTqP9VXjyzzeJk19Y78k0vRL4qvPAIDN8KCgx4MWPvT5mOjYJ2C/GO2HP0FL7lA0A58bFlZzyaUvt0A1gIobGDMlq4hZn86+VbHNwHOHAgMBAAECgYApFK4/AY3EHDVIX9UPajSJe3wDADPYXNr2wUSNfKK3fSPT26Z21dAKFT21WfnFiOl5Vlbxr3J90956YqUHg0yUTU0HZwnyiOZUs++HqvwCxI89K9vDDU56p7wMok8VYlK11iq5qOFcbHwkWOzWMstO1C0xhXak6sAWVE3QEJLK4QJBAPKwhZ/REs1pyAZ7dWw4cZg9SH+yuabN7xmkIB/72QhfW8kQuAJHJmFUVYcL40LefiX1WMPtYZxtEy6pQVh4YhMCQQDe6vVNRwRAgaGBNCblEpwhc9q2AJHfvHgIL1lz41z7nottlwkarXvUjohiJ7k6TH5DgMeFL4PeIemHZFV75Tc9AkBi4yqPhFF1AbVPbt7ZyeX/cnkoD1s2bJMQj5/HaId2seRkeTuBMhyhbAuti1p+1MnyDZatvvwXERrP2IeX1PWhAkAeWck6U0/95Ghz6gDdsZH2cAq4Ow2hc8y3+JJqV+uCCs0bB7RioHo6r4lcK+JpUJ7kW/VOafC/Xsin+X42FzhVAkEAjH+/seJOQ2lXCg9R/7Fl/7/OGrs5cgg6Iyw7e6d5Wo5grUckwKeUuAkRFnpf/hic5s3soMpT+vAQwPXl8Ygm9A==',
//				// 原配publickey
////				publicKey: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDTU9HG7bCKcs5Wv7o3xEdy29xIQ1UhXtS/3S7VqlKMqA3YSIvnz+l+DnawLfU4PL6y6jk6j/VV48s83iZNfWO/JNL0S+KrzwCAzfCgoMeDFj70+Zjo2Cdgvxjthz9BS+5QNAOfGxZWc8mlL7dANYCKGxgzJauIWZ/OvlWxzcBzhwIDAQAB',
//				// 富友返回publickey
//				publicKey: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDVPZ86OFLLhEs5JdcwErGkpUuVU79Ge+UoGUyaB0Rufc+Ib1X/vb9j0fo0yaxK6ERelCIoVYothCf86ZvTprWgYOp4/1KOQ87EjDHK3nj1tvmZa0Pd9mHdDYjSUuxMQNQKXgPIjDR4e1PG0CxhZ5u4HVdoI/hlPRaLQAo6ubJNdQIDAQAB',
				privateKey: 'MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBANXuSuyjm5addHTMzatsB/4pghQNxjhcqiKRsgLMThycppMV66rnXdDZDQqL72Ms4GvNz7hJ65jcGtilZ6+mcd9Mft9tFSIux5uYSw0F+2i7a/LaelmMydAjxYc+vB1fyXqenfnBFR/VWvekgjVpdZwp6UsA4T8ojbkFAvR3eeszAgMBAAECgYAgrpLLrALs84kC/va8pXhjH7w7jA8oNR/YwCFNxCB6xoO8HLEwHsB6U25mmhIsOILgdUSyhrhzVczUJBC/CNKEXwnrPA9Cj9bbNDAp4272O8xfYm+VVyXbLgr3ninyP6/VZXPNcEyTqKCKw3tgrIAz8TcF12TM+slWu0ArLJjgwQJBAO9Ebb6TiiHbcVkJ3teO8eNjtvaU25dLt6vEff9CRdkry3G4fqNWZOj+DSP++K+/RL1dyegFqPXgj73qhc54QxsCQQDk5ES7OiMrq/WZRlfrt5hsKlAr7RD2vYzc5ZVWwVK6atMk41yIXmLU77MRLiN3T9ktd2E6IfGk9o64wOxW4GHJAkEAoDrDA4zqRCIgle/2O4SOmCh+rkKhi4A5T94WQoqvG/AoHc/ffqKskRsQ0bo97/O2pNnaDHc93uqzWAtl8A65YQJBAJZY+myg+4ksIt7TPd0vc4dZI+A2j/YC/Av9IeMiiVy53odUcOuCVBjaehzG2bT9VgJvcAXnVfqtmYHx90v5h0ECQQCtp5+hH60058Wt5n/e8bXsflHGBdJohkGW+AwzrVhDwrKFlU+2Jep9bm6DthWHvflJMr/OGH3sg3WyVa3gP/67',
				// 原配publickey
//				publicKey: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDV7krso5uWnXR0zM2rbAf+KYIUDcY4XKoikbICzE4cnKaTFeuq513Q2Q0Ki+9jLOBrzc+4SeuY3BrYpWevpnHfTH7fbRUiLsebmEsNBftou2vy2npZjMnQI8WHPrwdX8l6np35wRUf1Vr3pII1aXWcKelLAOE/KI25BQL0d3nrMwIDAQAB',
				// 富友返回publickey
				publicKey: 'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDowY9k4d1cvJM5ItWxe6dIPgoM68TGp83401yr9zS0PMo9/uSeXehm9K0jf6zdiJMkyG0Ezhcf4z0pnu1YN2CAvcrzJEdSKx4JVWSs5mmOlBdtwaSlWf5gdIcyGneiWCoHGP4DnrkWj6eb+Q4imjIYBB+kPKkBr7Np7z6SDzGIoQIDAQAB',
			},
		}
	};

	return {
		...config,
		...userConfig,
	};
};