const Service = require('egg').Service;

class SplitLog extends Service {
	
	// 添加日志
	async add(pcId, pslType, poServerOrderId, pslSplitId, pslMoney, pslFinancingReturn, 
				pslMerchantId, pslMerchantMoney, pslMerchantRate, pslParentId, pslParentMoney, 
				pslParentRate, createUser) {
		
		const result = await this.app.mysql.insert('pay_split_log', {
			pcId: pcId,
			pslType: pslType,
			poServerOrderId: poServerOrderId,
			pslSplitId: pslSplitId,
			pslMoney: pslMoney,
			pslFinancingReturn: pslFinancingReturn,
			pslMerchantId: pslMerchantId,
			pslMerchantMoney: pslMerchantMoney,
			pslMerchantRate: pslMerchantRate,
			pslParentId: pslParentId,
			pslParentMoney: pslParentMoney,
			pslParentRate: pslParentRate,
			createUser: createUser,
		});
		
		return result.insertId;
	}
					
	
	
}

module.exports = SplitLog;