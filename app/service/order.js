const Service = require('egg').Service;
const moment = require('moment');

class OrderService extends Service {
	
	// 添加订单信息
	async add(pcId, poCustomOrderId, poIp, poOpenid, poDescribe, 
					poPrice, poServerOrderId, poCustomInfo, poAllInfo, 
					poServerId, pcType, poPaySource) {
		
		const result = await this.app.mysql.insert('pay_order', {
			pcId: pcId,
			poCustomOrderId: poCustomOrderId,
			poIp: poIp,
			poOpenid: poOpenid,
			poDescribe: poDescribe,
			poPrice: poPrice,
			poServerOrderId: poServerOrderId,
			poCustomInfo: poCustomInfo,
			poAllInfo: poAllInfo,
			poServerId: poServerId,
			pcType: pcType,
			poPaySource: poPaySource
		});
		
		return result.insertId;
	}
					
	// 通过订单号获取订单信息
	async getInfoFromOrderId(pcId, poCustomOrderId) {
		
		const poInfo = await this.app.mysql.get('pay_order', {pcId: pcId, poCustomOrderId: poCustomOrderId});
		
		return poInfo;
	}
	
	// 设置订单状态
	async setRefund(pcId, poCustomOrderId){
		
		// 如果主键是自定义的 ID 名称，如 custom_id，则需要在 `where` 里面配置
		const row = {
			poRefundTime: moment().format('YYYY-MM-DD HH:mm:ss'),
			poStatus: 3
		};
		
		const options = {
		    where: {
		      pcId: pcId,
		      poCustomOrderId: poCustomOrderId
		    }
		};
		
		await this.app.mysql.update('pay_order', row, options); // 更新 posts 表中的记录
	}
	
}

module.exports = OrderService;