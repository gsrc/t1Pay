const Service = require('egg').Service;

class CustomerService extends Service {
	
	// 获取商户信息
	async getInfoKey(pcKey) {
		
		const info = await this.app.mysql.get('pay_customer', { pcKey: pcKey });
		if (!info){
			return false;
		}
		return info;
	}
	
}

module.exports = CustomerService;