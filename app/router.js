'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
	const {
		router,
		controller
	} = app;
	router.get('/', controller.home.index);

	router.post('/test', controller.home.test);
	router.post('/payPage', controller.home.payPage);
	// 创建订单
	router.post('/tkOrderCreate', controller.index.tkOrderCreate);
	// 支付回调
	router.post('/tkPayCallback/:payChannel', controller.index.tkPayCallback);
	// 订单退款
	router.post('/tyOrderRefund', controller.index.tyOrderRefund);
	
	// 富友创建分账进件
	router.post('/allocationCompany', controller.fySplitAccount.allocationCompany);
	// 富友创建分账规则
	router.post('/splitRuleCreate', controller.fySplitAccount.splitRuleCreate);
	// 富友订单分账
	router.post('/orderSplit', controller.fySplitAccount.orderSplit);
	// 富友订单分账撤销
	router.get('/orderSplitCancel', controller.fySplitAccount.orderSplitCancel);
	// 富友电商分账-微信配置
	router.post('/setWxConfig', controller.fySplitAccount.setWxConfig);
};