'use strict';

const moment = require('moment');
const BaseController = require('./base');

class IndexController extends BaseController {
		
	// 创建订单
	async tkOrderCreate() {
		
		const {
			ctx
		} = this;

		// 记录请求日志
		const logId =  await this.addReqLog();

		// 获取post参数
		const requestObj = ctx.request.body;
		
		const customInfo = await this.getCustomInfo();
		if (!customInfo){
			// 更新请求返回结果
			this.setReqLog(logId, JSON.stringify(ctx.body));
			return;
		}
		
		ctx.body = await this.returnMsg(1, 'OK', customInfo);
		
		var returnTemplate = {};
		returnTemplate.tkOrderId = "";
		returnTemplate.payKey = customInfo.pcKey;
		returnTemplate.payData = {};
		
		if (customInfo.pcType == 'fy'){           // 富友支付
			var orderInfo = await ctx.service.fuyou.createOrder(requestObj, customInfo);
			// 获取订单信息失败
			if (orderInfo.result_code != 0 || orderInfo.result_msg != 'SUCCESS'){
				ctx.body = await this.returnMsg(0, orderInfo.result_msg);
				// 记录请求返回
				this.setReqLog(logId, JSON.stringify(ctx.body));
				return;
			}
			
			// 记录订单
			if (!requestObj.customOrderId){
				requestObj.customOrderId = "FY" + moment().utc().utcOffset(8).format("YYYYMMDDHHmmss") + "XD" + Math.ceil(Math.random()*10000) + "C";
			}
			ctx.service.order.add(customInfo.pcId, requestObj.customOrderId, requestObj.clientIp, 
										requestObj.openid, requestObj.orderDesc, requestObj.payMoney,
					                    orderInfo.mchnt_order_no, requestObj.customInfo, 
					                    JSON.stringify(orderInfo), '', customInfo.pcType, requestObj.payChannel);
			
			returnTemplate.customOrderId = requestObj.customOrderId;
			returnTemplate.tkOrderId = orderInfo.mchnt_order_no;
			returnTemplate.payData = orderInfo.reserved_pay_info;
			
			ctx.body = await this.returnMsg(1, 'OK', returnTemplate);
			// 更新请求返回结果
			this.setReqLog(logId, JSON.stringify(ctx.body));
			return;
		} else if (customInfo.pcType == 'tf'){    // 天府银行
			
		} else if (customInfo.pcType == 'tl'){    // 通联支付
			
		}
		
		// 记录请求返回
		this.setReqLog(logId, JSON.stringify(ctx.body));
	}
	
	// 回调
	async tkPayCallback(){
		
		const {
			ctx
		} = this;
		
		// 记录请求日志
		const logId =  await this.addReqLog();
		// 获取支付渠道
		const payChannel = ctx.params.payChannel;
		
		if (payChannel == 'fy'){
			var result = await ctx.service.fuyou.payCallback();
			ctx.body = '1';
			// 记录请求返回
			this.setReqLog(logId, ctx.body);
			return;
		} else if (payChannel == 'tf'){
			
		} else if (customInfo.pcType == 'tl'){    // 通联支付
			
		}

		ctx.body = 'OK';
	}
	
	// 退款
	async tyOrderRefund(){
		
		const {
			ctx
		} = this;

		// 记录请求日志
		const logId = await this.addReqLog();

		// 获取post参数
		const requestObj = ctx.request.body;
		
		const customInfo = await this.getCustomInfo();
		if (!customInfo){
			// 更新请求返回结果
			this.setReqLog(logId, JSON.stringify(ctx.body));
			return;
		}

		if (customInfo.pcType == 'fy'){           // 富友支付
			var refundInfo = await ctx.service.fuyou.orderRefund(requestObj, customInfo);
			if (refundInfo === true){
				ctx.body = await this.returnMsg(1, 'OK');
				// 更新退单信息
				ctx.service.order.setRefund(customInfo.pcId, requestObj.customOrderId);
			}else{
				ctx.body = await this.returnMsg(0, refundInfo);
			}
			
			// 更新请求返回结果
			this.setReqLog(logId, JSON.stringify(ctx.body));
			return;
		} else if (customInfo.pcType == 'tf'){    // 天府银行
			
		} else if (customInfo.pcType == 'tl'){    // 通联支付
			
		}
		
		// 记录请求返回
		this.setReqLog(logId, JSON.stringify(ctx.body));
		
	}

}

module.exports = IndexController;