'use strict';

const Controller = require('egg').Controller;
const urlencode = require('urlencode');
const urldecode = require('urldecode');

class HomeController extends Controller {
	async index() {
		const {
			ctx
		} = this;
		ctx.body = 'hi, egg';
	}

	async test() {
		const {
			ctx
		} = this;

		var path = require("path");

		var url = path.resolve('./');
		ctx.body = url;
		return;
		console.log(ctx.request.originalUrl);
		console.log(ctx.request.url);
		console.log(ctx.request.origin);
		return;
		//  console.log(ctx);
		//  console.log('///////////////////');
		//  console.log(ctx.req);
		//  console.log('///////////////////');
		//console.log(ctx.request);
		var str1 = ">测试订单</";
		//%3e%b2%e2%ca%d4%b6%a9%b5%a5%3c%2f
		//%253E%25B2%25E2%25CA%25D4%25B6%25A9%25B5%25A5%253C%252F
		//    %253E%25B2%25E2%25CA%25D4%25B6%25A9%25B5%25A5%253C%252F
		//  ctx.body = urlencode(urlencode(str1));

		//  ctx.body = encodeURIComponent(encodeURIComponent(str1));
		//%253E%25E6%25B5%258B%25E8%25AF%2595%25E8%25AE%25A2%25E5%258D%2595%253C%252F
		//  ctx.body = urlencode(encodeURIComponent(str1));
		//%253E%25E6%25B5%258B%25E8%25AF%2595%25E8%25AE%25A2%25E5%258D%2595%253C%252F
		//  ctx.body = encodeURIComponent(urlencode(str1));
		//%253E%25E6%25B5%258B%25E8%25AF%2595%25E8%25AE%25A2%25E5%258D%2595%253C%252F
		//  ctx.body = urlencode(str1);
		//%3E%E6%B5%8B%E8%AF%95%E8%AE%A2%E5%8D%95%3C%2F
		//  ctx.body = encodeURIComponent(str1);
		//%3E%E6%B5%8B%E8%AF%95%E8%AE%A2%E5%8D%95%3C%2F
		ctx.body = urlencode(str1, 'gbk');
		//  %3E%B2%E2%CA%D4%B6%A9%B5%A5%3C%2F
		ctx.body = urlencode(urlencode(str1, 'gbk'));
		//		%253E%25B2%25E2%25CA%25D4%25B6%25A9%25B5%25A5%253C%252F
		return;

		console.log(ctx.request.originalUrl);
		console.log(ctx.request.url);
		console.log(ctx.request.origin);

		//  var str = "addn_inf=&curr_type=CNY&goods_des=Cold Drink100ml&goods_detail=&goods_tag=&ins_cd=08M0061520&mchnt_cd=0002230F0348879&mchnt_order_no=991501572645&notify_url=http://www-1.fuiou.com:28090/gpay/wg/atCallback.fuiou&order_amt=1&order_type=ALIPAY&random_str=2059802e25d44a9&term_id=I7HhL0fM&term_ip=115.112.59.162&txn_begin_ts=20170801152328&version=1.0";
		//  var str = "addn_inf=desc&curr_type=CNY&goods_des=test&goods_detail=&goods_tag=&ins_cd=08M0026945&limit_pay=&mchnt_cd=0006510F2756892&mchnt_order_no=143205020191008135618?ify_url=http://api.bapushop.com/notify/fynotice&openid=&order_amt=1&product_id=&random_str=1570514659&sub_appid=&sub_openid=op3ezjs6t5ZGCnb7M3U1699S08-8&term_id=88888888&term_ip=127.0.0.1&trade_type=JSAPI&txn_begin_ts=20191008140419&version=1";
		//  var str = "addn_inf=订单描述&curr_type=CNY&goods_des=测试订单&goods_detail=&goods_tag=&ins_cd=08M0026945&limit_pay=&mchnt_cd=0006510F2756892&mchnt_order_no=143205020191008230137¬ify_url=http://api.bapushop.com/notify/fynotice&openid=&order_amt=1&product_id=&random_str=1570547109&sub_appid=&sub_openid=op3ezjs6t5ZGCnb7M3U1699S08-8&term_id=88888888&term_ip=127.0.0.1&trade_type=JSAPI&txn_begin_ts=20191008230509&version=1"
		var str = "addn_inf%3d%e8%ae%a2%e5%8d%95%e6%8f%8f%e8%bf%b0%26curr_type%3dCNY%26goods_des%3d%e6%b5%8b%e8%af%95%e8%ae%a2%e5%8d%95%26goods_detail%3d%26goods_tag%3d%26ins_cd%3d08M0026945%26limit_pay%3d%26mchnt_cd%3d0006510F2756892%26mchnt_order_no%3d143205020191008230137%26notify_url%3dhttp%3a%2f%2fapi.bapushop.com%2fnotify%2ffynotice%26openid%3d%26order_amt%3d1%26product_id%3d%26random_str%3d1570547109%26sub_appid%3d%26sub_openid%3dop3ezjs6t5ZGCnb7M3U1699S08-8%26term_id%3d88888888%26term_ip%3d127.0.0.1%26trade_type%3dJSAPI%26txn_begin_ts%3d20191008230509%26version%3d1";

		// AKoUNj1H3rPDGLQI0Vg3PFlVZBLKML3cMDJGUdCJrZ4Leudechae0Ao5jeyQhkPMqxSbo+xzqfvTty4PcUW4F6yZzKf39OYBb6kETYAm6NPPx2/nsItRhCH98p4XkcYx1ebiHtMe8qO4cJGlSjH94gGuCDRMg7YlmKe7L0YCyPk=
		// ANh1n7ChV2LpER/QEIvw3u/EbhfuTYn4M0iLw6vNV5r3HwL8VC3cM6QjG9momhmeJrayK2cxiId2GREVWAQ/nrlKrpSayTA7ByQ0Wbm30DPOCklF3L7Ue21LiH2AeBc7jLgpdYz0ZYDBlGkuRuiQJA4LaPSXJV+dXO2xFs1yMEU=
		console.log(urldecode(str));
		var sign = await ctx.service.fuyou.getSign(urldecode(str));
		console.log(sign);
		ctx.body = sign;
	}

	async payPage() {
		
		const {
			ctx
		} = this;

		// 获取post参数
		//const requestObj = ctx.request.body;

//		var requestObj = ctx.query;
		var requestObj = ctx.request.body;
		console.log('接受POST参数：');
		console.log(requestObj);
		requestObj.clientIp = requestObj.clientIp.replace(/X/g, ".");
		
		var postData = {};
		postData.pay = requestObj.payMoney/100;
		postData.desc = requestObj.orderDesc;
		postData.order_id=requestObj.customOrderId;
		postData.openid = requestObj.openid;
		postData.customInfo = requestObj.customInfo;
		postData.businessName = requestObj.businessName?requestObj.businessName:"";
		postData.successUrl = requestObj.successUrl;
		postData.failureUrl = requestObj.failureUrl;
		postData.payData = requestObj.payData;

		
		var pageUrl = "https://emedia.sbedian.com/api/pay/payPage";
		// 通知商户
		const payConfigResult = await ctx.curl(ctx.request.origin+'/tkOrderCreate', {
	        // 必须指定 method
	        method: 'POST',
	        // 通过 contentType 告诉 HttpClient 以 JSON 格式发送
	        contentType: 'json',
	        data: requestObj,                 
			timeout: 9000,
	        // 明确告诉 HttpClient 以 JSON 格式处理返回的响应 body
	        dataType: 'json',
	  });
	  console.log('payConfigResult:');
	  console.log(payConfigResult)
	  if (payConfigResult.data.code == 0){
	  	ctx.body = payConfigResult.data.msg;
	  	return;
	  }
	  postData.payData = payConfigResult.data.data.payData;
	  
	  var params = "";
	  for(var key in postData){
	  	params += "&" + key + "=" + urlencode(postData[key]);
	  }
	  params = params.substr(1);
	  
	  var reqUrl = pageUrl + "?" + params;
	  console.log(reqUrl);
	  ctx.redirect(reqUrl);
	}
}

module.exports = HomeController;