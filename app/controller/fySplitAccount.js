'use strict';

const moment = require('moment');
const BaseController = require('./base');

class FySplitAccountController extends BaseController {
	
	// 电商分账企业户开户
	async allocationCompany(){
		
		const {
			ctx
		} = this;
		
		var customInfo = await this.getCustomInfo();

		if (!customInfo){
			return;
		}
		
		var result = await ctx.service.fuyouSplitAccount.allocationCompany(customInfo);
		if (typeof result == "object"){
			customInfo.pcPayConfig.fuyou.splitLoginId = result.loginId;
			ctx.service.common.setCustomConfig(customInfo.pcId, customInfo.pcPayConfig);
			ctx.body = await this.returnMsg(1, 'OK');
			return;
		}else{
			ctx.body = await this.returnMsg(0, result);
			return;
		}
	}
	
	// 分账导入
	async splitRuleCreate(){
		
		const {
			ctx
		} = this;
		
		var customInfo = await this.getCustomInfo();
		if (!customInfo){
			return;
		}
		
		var result = await ctx.service.fuyouSplitAccount.splitRuleCreate(customInfo);
		if (typeof result == "object"){
			customInfo.pcPayConfig.fuyou.splitSsn = result.ruleInfo.splitSsn;
			ctx.service.common.setCustomConfig(customInfo.pcId, customInfo.pcPayConfig);
			ctx.body = await this.returnMsg(1, 'OK');
			return;
		}else{
			ctx.body = await this.returnMsg(0, result);
			return;
		}
	}

	// 订单分账
	async orderSplit(){
		
		const {
			ctx
		} = this;
		
		const customInfo = await this.getCustomInfo();
		
		// 获取post参数
		const requestObj = ctx.request.body;
		const customOrderId = requestObj.customOrderId;
		const orderInfo = await ctx.service.order.getInfoFromOrderId(customInfo.pcId, customOrderId);
		console.log('分账订单信息：');
		console.log(orderInfo);
		if (!orderInfo){
			ctx.body = await this.returnMsg(1, '找不到订单号');
			return;
		}
		
		var result = await ctx.service.fuyouSplitAccount.orderSplit(customInfo, orderInfo);
		if (result === true){
			ctx.body = await this.returnMsg(1, 'OK');
			return;
		}else{
			ctx.body = await this.returnMsg(0, result);
			return;
		}
	}
	
	// 取消分账
	async orderSplitCancel(){
		
		const {
			ctx
		} = this;
		
		const customInfo = await this.getCustomInfo();
		var result = await ctx.service.fuyouSplitAccount.orderSplitCancel(customInfo);
		if (result === true){
			ctx.body = await this.returnMsg(1, 'OK');
			return;
		}else{
			ctx.body = await this.returnMsg(0, result);
			return;
		}
	}
	
	// 微信参数配置
	async setWxConfig(){
		
		const {
			ctx
		} = this;
		
		const customInfo = await this.getCustomInfo();
		var result = await ctx.service.fuyouSplitAccount.setWxConfig(customInfo);
		if (result === true){
			ctx.body = await this.returnMsg(1, 'OK');
			return;
		}else{
			ctx.body = await this.returnMsg(0, result);
			return;
		}
	}
}

module.exports = FySplitAccountController
;